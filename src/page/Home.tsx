import { Breadcrumb, Button, Layout, Menu, theme } from "antd";
import "./Home.scss";
import { useNavigate, Route, Routes } from "react-router-dom";
import HomePage from "./detail/HomePage";
import Farming from "./detail/Farming";
import Community from "./detail/Community";
import PersonCenter from "./detail/PersonCenter";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
  WechatWorkOutlined,
  ProfileOutlined,
  AlignLeftOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import { useState } from "react";

const { Header, Content, Sider } = Layout; // 样式

// 菜单栏
const items = [
  {
    label: "主页",
    key: "homePage",
    icon: <WechatWorkOutlined />,
  },
  {
    label: "农业科谱",
    key: "farming",
    icon: <ProfileOutlined />,
  },
  {
    label: "社区分享",
    key: "community",
    icon: <AlignLeftOutlined />,
  },
  {
    label: "个人中心",
    key: "personCenter",
    icon: <HomeOutlined />,
  },
];

const Hom = (props: { setLogin: (val: boolean) => void }) => {
  const { setLogin } = props;
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  const navigate = useNavigate(); // 路由

  // 点击菜单栏
  const clickMenu = (e: any) => {
    navigate("/" + e.key);
  };

  const [collapsed, setCollapsed] = useState(false);
  // const {
  //   token: { colorBgContainer, borderRadiusLG },
  // } = theme.useToken();
  return (
    <Layout className="box1">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="demo-logo-vertical" />
        <Menu
          onClick={clickMenu}
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={items}
        />
      </Sider>
      <div className="homeContent">
        <Header
          className="head"
          style={{ padding: 0, background: colorBgContainer }}
        >
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
        </Header>
        <Content
          className="box3"
          style={{
            margin: "24px 16px",
            padding: "0 48px",
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          <div
            className="box4"
            style={{
              background: colorBgContainer,
              minHeight: 280,
              padding: 24,
              borderRadius: borderRadiusLG,
            }}
          >
            <Routes>
              <Route path="/homePage" element={<HomePage />} />
              <Route path="/farming" element={<Farming />} />
              <Route path="/community" element={<Community />} />
              <Route
                path="/personCenter"
                element={<PersonCenter setLogin={setLogin} />}
              />
            </Routes>
          </div>
        </Content>
      </div>
    </Layout>
  );
};

export default Hom;

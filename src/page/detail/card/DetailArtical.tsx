import "./DetailArtical.scss";
import type { cardT } from "./CardCom";
import { useEffect } from "react";
import { postLookAdd, postLook } from "@s/api/apis";
function DetailArtical(props: {
  data: cardT;
  setArtical: (val: null | cardT) => void;
}) {
  const { data, setArtical } = props;
  function goReturn() {
    setArtical(null);
  }

  useEffect(() => {
    postLookAdd({
      Uid: localStorage.getItem("Uid"),
      Aid: data.Aid,
    });
    postLook({
      Aid: data.Aid,
    });
  }, []);
  return (
    <>
      {/* 返回按钮 */}
      <div onClick={goReturn} className="articalRetrun">
        返回
      </div>
      {/* 文章主题 */}
      <div className="articalToot">
        <h1>{data.title}</h1>
        <div>{data.content}</div>
        <div>
          <div>{data.date}</div>
          <div>{data.author}</div>
        </div>
      </div>
    </>
  );
}

export default DetailArtical;

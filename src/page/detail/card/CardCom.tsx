import "./CardCom.scss";
import { Divider } from "antd";
import eyeImg from "../../../assets/eye.png";
import collectImg from "../../../assets/collect.png";
import noCollectImg from "../../../assets/noCollect.png";
import { message } from "antd";
import { postCollect, postNoCollect } from "@s/api/apis";
export type cardT = {
  title: string; // 标题
  abstract: string; // 摘要
  date: string; // 日期
  author: string; // 作者名
  comment: number; // 评论数量
  Aid: number; // 文章id
  content: string; // 文章内容
  isCollect: Boolean; // 是否收藏
  numCollect: number; // 收藏总数量
};
const CardCom = (props: { data: cardT; getAllArtical: () => void }) => {
  const { data, getAllArtical } = props;
  const [messageApi, contextHolder] = message.useMessage();

  function successAlert(e:any) {
    // 成功 （取消收藏，收藏）、 失败
    // 请求是否成功
    if (!data.isCollect) {
      postCollect({
        Uid: Number(localStorage.getItem("Uid")),
        Aid: data.Aid,
      }).then((res: any) => {
        if (!res) {
          messageApi.open({
            type: "error",
            content: "重复收藏",
          });
        } else {
          messageApi.open({
            type: "success",
            content: "收藏成功",
          });
        }
        getAllArtical();
      });
    } else {
      postNoCollect({
        Uid: Number(localStorage.getItem("Uid")),
        Aid: data.Aid,
      }).then((res: any) => {
        if (!res) {
          messageApi.open({
            type: "error",
            content: "重复收藏",
          });
        } else {
          messageApi.open({
            type: "success",
            content: "取消收藏成功",
          });
        }
        getAllArtical();
      });
    }

    e.stopPropagation();
  }
  return (
    <>
      <div className="itemBox">
        {contextHolder}
        {/* 标题 */}
        <div className="top">{data.title}</div>
        {/* 摘要 */}
        <div className="center">{data.abstract}</div>
        {/* 信息 */}
        <div className="bottom">
          <div>
            <img src={eyeImg} />
            {data.comment}
            <div className="collectIcon" onClick={successAlert}>
              <img
                style={{
                  marginLeft: "20px",
                  width: "17px",
                  height: "17px",
                }}
                src={data.isCollect ? collectImg : noCollectImg}
              />
              {data.numCollect}
            </div>
          </div>
          <div>{data.date}</div>
          <div>{data.author}</div>
        </div>
      </div>
      <Divider />
    </>
  );
};

export default CardCom;

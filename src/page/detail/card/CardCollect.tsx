import "./CardCollect.scss";
import { cardT } from "./CardCom";
import Card from "antd/es/card/Card";
import { Divider } from "antd";
import delectIcon from "@s/assets/delectIcon.png";
import { postNoCollect } from "@s/api/apis";
import { message } from "antd";
export default function CardCollect(props: {
  id: string | null;
  data: cardT;
  getAllCollect: () => void;
}) {
  const { data, getAllCollect, id } = props;
  const [messageApi, contextHolder] = message.useMessage(); // 提示警告
  function cancelCollect() {
    if (id == "my") {
      messageApi.open({
        type: "success",
        content: "文章创造不易，珍惜文章，请勿删除",
      });

      return;
    }

    postNoCollect({
      Uid: Number(localStorage.getItem("Uid")),
      Aid: data.Aid,
    }).then((res: any) => {
      if (!res) {
        messageApi.open({
          type: "error",
          content: "重复收藏",
        });
      } else {
        messageApi.open({
          type: "success",
          content: "取消收藏成功",
        });
      }
      getAllCollect();
    });
  }
  return (
    <Card className="CardCollectRoot">
      {contextHolder}
      <div className="delectCollect">
        <img
          onClick={cancelCollect}
          style={{
            width: "20px",
          }}
          src={delectIcon}
        ></img>
      </div>
      {/* 标题 */}
      <div className="top">{data.title}</div>
      <Divider></Divider>
      {/* 摘要 */}
      <div className="center">{data.content}</div>
      {/* 信息 */}
      <div className="bottom">
        <div>{data.date}</div>
        <div>{data.author}</div>
      </div>
    </Card>
  );
}

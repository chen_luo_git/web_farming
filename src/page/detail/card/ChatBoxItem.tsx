import "./ChatBoxItem.scss";
import type { chatList } from "../HomePage";
import { status } from "../HomePage";
import catImg from "../../../assets/cat.png";
import dogImg from "../../../assets/dog.png";
import { Card } from "antd";

function ChatBoxItem(props: { data: chatList }) {
  const { data } = props;

  return (
    <>
      <>
        <div
          className={
            data.Speaker == "AI"
              ? "chatBoxItemRootLeft"
              : "chatBoxItemRootRight"
          }
        >
          {/* 头像 */}
          <div className="headImg">
            <img src={data.Speaker == "AI" ? catImg : dogImg}></img>
          </div>
          <div className="contentBox">
            {/* 日期 */}
            <span className="date">{data.DialogueDate}</span>
            {/* 对话内容 */}
            <div className="content">
              <Card>{data.DialogueContent}</Card>
            </div>
          </div>
        </div>
      </>
    </>
  );
}
export default ChatBoxItem;

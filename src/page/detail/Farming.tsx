import "./HomePage.scss";
import { Image } from "antd";
import { Carousel } from "antd";
import data from "@u/dataCrop";
import type listItem from "@u/dataCrop";
import { Card } from "antd";
import { Input } from "antd";
import { useEffect, useState } from "react";
import lightUrl from "../../assets/carouse/light.png";
import shifeiUrl from "../../assets/carouse/shifei.png";
import tudiUrl from "../../assets/carouse/tudi.png";
import waterUrl from "../../assets/carouse/water.png";
import Item from "antd/es/list/Item";

const contentStyle: React.CSSProperties = {
  height: "300px",
  color: "#fff",
  lineHeight: "300px",
  textAlign: "center",
  background: "#364d79",
};
const { Search } = Input;
const HomePage = () => {
  const [showList, setShowList] = useState(false); // 是否展示搜索列表
  const [detail, setDetail] = useState<listItem>(false); // 是否详细介绍
  const [listData, setListData] = useState([
    { name: "苹果", id: 1 },
    { name: "金冠苹果", id: 2 },
    { name: "富士山苹果", id: 3 },
    { name: "青苹果", id: 4 },
  ]); //搜索模糊查询列表
  const [searchName, setSearchName] = useState(""); // 搜索值
  const carouselData = [
    {
      title: "浇水小知识",
      imgUrl: waterUrl,
      detail:
        "农作物种类繁多，其对水分的需求与耐旱性各有差异，直接影响产量与品质。水稻属水生作物，需水量大，适宜湿润环境，缺水易导致植株矮小、穗粒减少。小麦、玉米等禾谷类作物虽较耐旱，但仍需适量水分以保证分蘖、抽穗与灌浆，缺水可能导致籽粒瘪瘦、产量下降。豆科作物如大豆、豌豆，对水分需求适中，过湿易引发病害，过旱则影响开花结荚。蔬菜中，叶菜类如菠菜、白菜需水较多，保持土壤湿润利于快速生长；根茎类如马铃薯、胡萝卜则对水分敏感，过湿易致烂根，适度干旱利于块茎膨大。果树如柑橘、需水充足但忌积水，合理灌溉利于果实发育与品质提升。总体而言，根据不同作物的水分需求特性进行科学灌溉管理，既能节约水资源，又能保障作物健康生长，实现优质高产。",
    },
    {
      title: "光照很重要",
      imgUrl: lightUrl,
      detail:
        "光照是农业生产中至关重要的因素，不同农作物对光照强度、光照时长的需求各异，直接影响光合作用效率、生长发育进程与产量品质。水稻喜光，充足的光照有利于叶片光合产物积累，促进植株健壮、分蘖增多、穗大粒饱。小麦、玉米等禾谷类作物虽有一定耐阴性，但长期弱光易导致植株徒长、籽粒灌浆不足，影响产量与营养价值。豆科作物如大豆、豌豆对光照需求较高，充足的阳光有助于固氮菌活动、提高生物固氮效率，促进植株营养吸收与生长发育。叶菜类蔬菜如菠菜、白菜喜光，强光有助于叶绿素合成、增强光合效能，使叶片色泽鲜亮、口感脆嫩。而部分果菜如番茄、辣椒则对光照敏感，长时间强光易诱发日灼病，适量遮荫或调控设施内光照强度至关重要。果树如柑橘、苹果等，适宜光照强度与持续时间有助于花芽分化、果实着色与糖分积累，提升果实品质。综上，针对各类农作物的光照需求特性，合理安排种植布局、调控光照条件，对实现作物高效生产、提升农产品质量至关重要。",
    },
    {
      title: "施肥有学问",
      imgUrl: shifeiUrl,
      detail:
        "施肥是农业生产的基石，对农作物生长发育、产量提升与品质优化具有决定性影响。不同农作物对养分的需求结构、数量各不相同，科学合理的施肥策略至关重要。水稻生长周期长，需肥量大，尤其对氮、磷、钾需求均衡，适量施用基肥与追肥，能有效促进分蘖、壮秆、防倒伏，确保穗大粒多、产量稳定。小麦、玉米等禾谷类作物，氮肥对其生长尤为关键，适量施用可促进茎秆粗壮、穗大粒满；磷、钾肥则有助于根系发达、增强抗逆性、改善籽粒品质。豆科作物如大豆、豌豆具有固氮能力，氮肥需求相对较低，但适量补充磷、钾及微量元素，可显著提升固氮效率、促进植株健壮、增加结荚数与籽粒饱满度。蔬菜中，叶菜类如菠菜、白菜需氮肥较多，以促进叶片生长、保持鲜绿；根茎类如马铃薯、胡萝卜对钾肥需求突出，有助于块茎、根系膨大、提高产量与耐贮性。果树如柑橘、苹果，全生育期需均衡施用氮、磷、钾肥，配合施用钙、镁、硼等中微量元素，利于树势健壮、花芽分化、果实膨大、改善风味与耐贮性。总的来说，根据不同农作物的养分需求特性和生长阶段，实施精准施肥、平衡施肥，既能满足作物生长所需，防止养分过剩或缺乏导致的生理障碍，又能提高肥料利用率、保护土壤环境，实现作物高产、优质、高效、环保的综合目标",
    },
    {
      title: "合理用土",
      imgUrl: tudiUrl,
      detail:
        "土地分类按所有权分为国有土地（优点：使用权稳定、便于宏观调控与规划，缺点：可能效率低下、流转受限）与集体土地（优点：维护农民权益与乡土文化，缺点：产权复杂、流转困难）。按用途分为农用地（优点：保障粮食安全、生态平衡，缺点：转用限制严格、可能效率不高、过度开发导致退化污染）、建设用地（优点：推动城市化、满足建设需求，缺点：占用农地、可能导致耕地减少、城乡差距、城市病）与未利用地（优点：潜在开发价值、土地储备，缺点：开发难度大、成本高、可能破坏生态）。按自然属性分为土壤类型（优点：指导农业生产，缺点：土壤类型可能变化）、地形地貌（优点：规划土地利用格局，缺点：复杂地区开发难度大、成本高）与气候带（优点：指导种植结构调整，缺点：气候变化影响分类适用性）。按经济属性分为土地等级（优点：为定价、交易、征税提供依据，缺点：等级评定主观性强、易受市场波动影响）。按管理目的分为土地规划分类（优点：利于城市有序发展、协调用地关系，缺点：规划调整导致用途不稳定、影响投资者信心）。",
    },
  ];
  // 搜索变化
  const onChange = (e: any) => {
    // e.target.value当前的值
    if (e.target.value == "苹") {
      setShowList(true);
    } else {
      setShowList(false);
    }
  };
  // 点击搜索
  const onSearch = (e: any) => {
    // e直接就是输入框的值
    setSearchName(e);
  };
  // 点击单个搜索结果
  const clickItem = (e: any) => {
    setSearchName(e.name);
  };

  useEffect(() => {
    const a = data.find((item: any) => {
      return item.name == searchName;
    });
    if (a) {
      setDetail(a);
    }
  }, [searchName]);
  return (
    <>
      <div className="HomePageRoot">
        {/* 轮播图 */}
        <Carousel dotPosition={"top"} autoplay className="Carousel">
          {carouselData.map((item: any) => {
            return (
              <div style={contentStyle}>
                <Image
                  preview={{
                    minScale: 1,
                    maxScale: 1,
                  }}
                  style={{
                    borderRadius: "10px",
                  }}
                  width={400}
                  height={200}
                  src={item.imgUrl}
                />
                {/* 下方描述 */}
                <Card className="describe">
                  <h2>{item.title}</h2>
                  {item.detail}
                </Card>
              </div>
            );
          })}
        </Carousel>

        {/* 右侧搜索 */}
        <div className="search">
          <Search
            allowClear
            onChange={onChange}
            onSearch={onSearch}
            style={{ width: "80%" }}
          />
          {showList ? (
            <Card
              className="cardSearchList"
              bordered={false}
              style={{ width: "80%" }}
            >
              {listData.map((item) => {
                return (
                  <div
                    onClick={() => clickItem(item)}
                    className="item"
                    key={item.id}
                  >
                    {item.name}
                  </div>
                );
              })}
            </Card>
          ) : (
            ""
          )}

          {/* 详细介绍  */}
          {detail ? (
            <Card
              className="detailCard"
              title={detail.name + "的详细介绍"}
              bordered={false}
              style={{ width: "100%" }}
            >
              <Image width={400} src={detail.picUrl}></Image>
              <div>{detail.describe}</div>
            </Card>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};

export default HomePage;

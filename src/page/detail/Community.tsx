import "./Community.scss";
import CardCom from "./card/CardCom";
import type { cardT } from "./card/CardCom";
import { useEffect, useState } from "react";
import DetailArtical from "./card/DetailArtical";
import { postAllArtical, postCollect } from "@api";
import { Button, Drawer } from "antd";

function Community() {
  const cards: cardT[] = [
    {
      title: "文章标题1",
      abstract: "这是文章摘要1，简要介绍主要内容绍主要内容绍主要内容。",
      date: "2023-01-01",
      author: "作者A",
      comment: 25,
      Aid: 1,
      isCollect: true,
      numCollect: 24,
      content:
        "111文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
    {
      title: "文章标题2",
      abstract: "这是文章摘要2，简要介绍主要内容绍主要内容绍主要内容。",
      date: "2023-07-08",
      author: "作者B",
      comment: 23,
      Aid: 2,
      isCollect: false,
      numCollect: 24,
      content:
        "222文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
    {
      title: "文章标题3",
      abstract: "这是文章摘要3，简要介绍主要内容绍主要内容绍主要内容。",
      date: "2023-03-15",
      author: "作者C",
      comment: 78,
      isCollect: false,
      numCollect: 24,
      Aid: 3,
      content:
        "333文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
    {
      title: "文章标题3",
      abstract: "这是文章摘要3，简要介绍主要内容绍主要内容绍主要内容。",
      date: "2023-03-15",
      author: "作者C",
      isCollect: false,
      numCollect: 24,
      comment: 78,
      Aid: 4,
      content:
        "444文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
    {
      title:
        "文章标题3绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容",
      abstract:
        "这是文章摘要3，简要介绍主要内容绍主要内容绍主要内容绍主要内容绍要内容绍主要内容绍主要内容绍要内容绍主要内容绍主要内容绍要内容绍主要内容绍主要内容绍要内容绍主要内容绍主要内容绍要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容绍主要内容。",
      date: "2023-03-15",
      author: "作者C",
      isCollect: false,
      numCollect: 24,
      comment: 78,
      Aid: 5,
      content:
        "555文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
    {
      title: "文章标题3",
      abstract: "这是文章摘要3，简要介绍主要内容。",
      date: "2023-03-15",
      author: "作者C",
      isCollect: false,
      numCollect: 24,
      comment: 78,
      Aid: 6,
      content:
        "666文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容",
    },
  ];

  const [allArtical, setAllArtical] = useState<cardT[]>(cards);
  const [showArtical, setShowArtical] = useState<null | cardT>(null);

  const setArtical = (val: null | cardT) => {
    setShowArtical(val);
  };
  function chooseArtical(val: cardT) {
    setShowArtical(val);
  }

  const getAllArtical = () => {
    postAllArtical({ userName: localStorage.getItem("userName") }).then(
      (res) => {
        res[0].content = `在农业领域，随着科技的飞速进步，智能农机、精准农业以及生物技术的应用正逐步重塑传统耕作方式，推动农业向现代化、智能化转型。以下是对这些关键技术的详尽探讨：
        智能农机装备：智能农机装备利用物联网（IoT）、全球导航卫星系统（GNSS）、人工智能（AI）等先进技术，实现了农业作业的自动化与智能化。这些装备包括自动驾驶拖拉机、无人机、智能喷洒机器人以及智能采摘机器人等。它们能够根据作物的具体需求和田间条件，执行精准播种、施肥、灌溉和病虫害防控等任务，显著提高作业效率，同时减少人力成本和资源浪费。例如，通过安装在农机上的传感器和GPS定位系统，可以实现作物生长状况的实时监控和精准导航，使得农机能够在无人干预的情况下高效作业。
        精准农业：精准农业是一种基于数据的决策支持系统，它利用GIS（地理信息系统）、遥感技术和大数据分析来优化农业生产管理。通过收集并分析土壤类型、气候条件、作物生长状态等多维度数据，精准农业能够为农民提供定制化的管理建议，如精确施肥、变量灌溉和病虫害预测，确保每一寸土地都能得到最适宜的管理，从而提高作物产量和品质，同时保护生态环境，促进农业可持续发展。  
        生物技术：生物技术在农业领域的应用主要涉及基因工程、分子标记辅助育种、生物农药和生物肥料等方面。通过基因编辑技术如CRISPR-Cas9，科学家能够培育出抗旱、抗病、高产或营养价值更高的作物品种，减少对化学农药和化肥的依赖。此外，生物技术还促进了微生物肥料和生物防治产品的开发，这些产品有助于恢复土壤健康，增强作物自然免疫力，进一步提升农业生态系统的稳定性和生产力。
        综上所述，智能农机、精准农业和生物技术的集成应用，不仅提升了农业生产效率和作物产量，也促进了资源的合理利用与环境保护，为实现农业的绿色转型和全球粮食安全提供了强有力的技术支撑。随着这些技术的不断成熟与创新，未来的农业将更加智慧、高效和可持续。";`;
        setAllArtical(res as cardT[]);
        // console.log(res, "返回的文章");
      }
    );
  };

  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  function setChat() {
    localStorage.setItem("chart", "现代农业如何发展");
  }

  useEffect(() => {
    getAllArtical();
  }, []);

  return (
    <>
      <div className="cardroot">
        <Drawer title="Basic Drawer" onClose={onClose} open={open}>
          <p
            onClick={() => {
              window.location.pathname = "/homePage";
            }}
          >
            现代农业如何发展
          </p>
          <p>未来的农业趋势</p>
          <p>如何提升农业知识</p>
        </Drawer>
        <div className="boxbox" onClick={showDrawer}>
          .
        </div>
        {!showArtical ? (
          allArtical.map((item: cardT) => {
            return (
              <div
                key={item.Aid}
                onClick={() => {
                  chooseArtical(item);
                }}
              >
                <CardCom getAllArtical={getAllArtical} data={item}></CardCom>
              </div>
            );
          })
        ) : (
          <DetailArtical
            setArtical={setArtical}
            data={showArtical}
          ></DetailArtical>
        )}
      </div>
      {/* <CardCom data={cards}></CardCom> */}
    </>
  );
}

export default Community;

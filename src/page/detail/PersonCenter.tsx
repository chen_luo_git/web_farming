import "./PersonCenter.scss";
import imgUrl from "../../assets/dog.png";
import { Divider } from "antd";
import type { CarouselProps, RadioChangeEvent } from "antd";
import { Carousel, Radio, Card, Button } from "antd";
import viewHistoryImg from "../../assets/viewHistory.png";
import myCollectImg from "../../assets/myCollect.png";
import myArticalImg from "../../assets/myArtical.png";
import penImg from "../../assets/pen.png";
import loginOutImg from "../../assets/loginOut.png";
import { useEffect, useState } from "react";
import { postUser } from "@s/api/apis";
import Mycollect from "./personCenterCard/Mycollect";
import MyArtical from "./personCenterCard/MyArtical";
import BrowseHistory from "./personCenterCard/BrowseHistory";
import WriteArtical from "./personCenterCard/WriteArtical";

export type userT = {
  Uid: number;
  password: number;
  userName: string;
  sex: string;
  intro: string; //简介
};
export enum stepPerson {
  Main = 1,
  MyArtical = 2,
  Mycollect = 3,
  BrowseHistory = 4,
  WriteArtical = 5,
}
const PersonCenter = (props: { setLogin: (val: boolean) => void }) => {
  const { setLogin } = props;
  const [userData, setUserData] = useState<userT | null>(null);
  const [step, setStep] = useState<stepPerson>(stepPerson.Main);
  // 退出登录
  const loginOut = () => {
    setLogin(false);
    localStorage.removeItem("userName");
    localStorage.removeItem("password");
    localStorage.removeItem("Uid");
  };
  const toSetStep = (val: stepPerson) => {
    setStep(val);
  };

  //主页面
  function MainPage() {
    return (
      <>
        <div className="percenCenterRoot">
          <div className="headImg">
            <img className="img" src={imgUrl}></img>
            <div className="nameBox">
              <div className="name">{userData?.userName}</div>
              <div className="signature">{userData?.intro}</div>
            </div>
            <Carousel className="zmd" dotPosition="left" autoplay>
              <div>你是最棒的</div>
              <div>你是最最棒的</div>
              <div>你是最最最最棒的</div>
              <div>你是最最最最最棒的</div>
            </Carousel>
            <Button
              onClick={() => toSetStep(stepPerson.WriteArtical)}
              className="rightArtical"
            >
              写文章
              <img src={penImg} style={{ width: "30px", height: "30px" }}></img>
            </Button>
          </div>

          <Divider></Divider>
          <Card>
            <div className="baseMsg">
              <div className="title">基本信息</div>
              <div className="itemMsg">
                <div>用户名称</div>
                <div>{userData?.userName}</div>
              </div>
              <div className="itemMsg">
                <div>用户ID</div>
                <div>{userData?.Uid}</div>
              </div>
              <div className="itemMsg">
                <div>性别</div>
                <div>{userData?.sex}</div>
              </div>
              <div className="itemMsg">
                <div>个人简介</div>
                <div>{userData?.intro}</div>
              </div>
            </div>
          </Card>
          <div className="bottomBox">
            <div
              onClick={() => toSetStep(stepPerson.MyArtical)}
              className="myArtical"
            >
              <img
                src={myArticalImg}
                style={{ width: "70px", height: "70px" }}
              ></img>
              我的文章
            </div>
            <div
              onClick={() => toSetStep(stepPerson.Mycollect)}
              className="myArtical"
            >
              <img
                src={myCollectImg}
                style={{ width: "70px", height: "70px" }}
              ></img>
              我的收藏
            </div>
            <div
              onClick={() => toSetStep(stepPerson.BrowseHistory)}
              className="myArtical"
            >
              <img
                src={viewHistoryImg}
                style={{ width: "70px", height: "70px" }}
              ></img>
              观看历史
            </div>
            <div className="loginOut">
              <img
                src={loginOutImg}
                style={{ width: "70px", height: "70px" }}
                onClick={loginOut}
              ></img>
              退出登录
            </div>
          </div>
        </div>
      </>
    );
  }
  // 条件渲染s
  function ChoosePage() {
    switch (step) {
      case stepPerson.Main:
        return <MainPage></MainPage>;
        break;
      case stepPerson.MyArtical:
        return <MyArtical toSetStep={toSetStep}></MyArtical>;
        break;
      case stepPerson.Mycollect:
        return <Mycollect toSetStep={toSetStep}></Mycollect>;
        break;
      case stepPerson.BrowseHistory:
        return <BrowseHistory toSetStep={toSetStep}></BrowseHistory>;
        break;
      case stepPerson.WriteArtical:
        return <WriteArtical toSetStep={toSetStep}></WriteArtical>;
        break;
      default:
        return <>页面加载异常</>;
        break;
    }
  }

  useEffect(() => {
    postUser({ userName: localStorage.getItem("userName") }).then(
      (res: any) => {
        setUserData(res[0] as userT);
      }
    );
  }, []);
  return (
    <>
      <ChoosePage></ChoosePage>
    </>
  );
};

export default PersonCenter;

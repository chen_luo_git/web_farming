import "./Farming.scss";
import { Button, Flex, Input } from "antd";

import { useEffect, useState } from "react";
import ChatBoxItem from "./card/ChatBoxItem";
import { getAllChat, postChat } from "@s/api/apis";
// 模型测试
import * as tf from "@tensorflow/tfjs";
import moment from "moment";
// import { GPT2LMHeadModel, GPT2Tokenizer } from "transformers";

// async function loadModel() {
//   const model = await GPT2LMHeadModel.fromPretrained("distilgpt2");
//   const tokenizer = new GPT2Tokenizer("distilgpt2");

//   return { model, tokenizer };
// }
// let { model, tokenizer } = await loadModel();

// function generateResponse(inputText) {
//   let inputIds = tokenizer.encode(inputText);
//   // 添加特殊标记，根据实际情况调整
//   inputIds = tf.tensor([inputIds]);

//   const output = model.generate(inputIds, {
//     max_length: 100,
//     num_return_sequences: 1,
//     no_repeat_ngram_size: 2,
//     do_sample: true,
//     temperature: 0.7,
//   });

//   let generatedText = tokenizer.decode(output[0]);
//   return generatedText;
// }

// console.log(generateResponse("你好，有什么可以帮助你的吗？"));

const { TextArea } = Input;

export enum status {
  AI = 1,
  USER = 2,
}

export type chatList = {
  DialogueContent: string; // 对话内容
  Speaker: status; // 人机orai
  DialogueDate: string; // 日期
};

const chatListInstance: chatList[] = [
  {
    DialogueContent: "您好，我是AI助手，请问有什么可以帮助您的？",
    Speaker: 1,
    DialogueDate: "2023-04-5 10:30:00",
  },
  {
    DialogueContent: "我想查询一下最近的天气情况。",
    Speaker: 2,
    DialogueDate: "2023-04-5 10:31:00",
  },
  {
    DialogueContent: "好的，为您查询到今天晴朗，气温20℃~28℃，请注意防晒。",
    Speaker: 1,
    DialogueDate: "2023-04-5 10:32:00",
  },
  {
    DialogueContent: "非常感谢！",
    Speaker: 2,
    DialogueDate: "2023-04-5 10:33:00",
  },
];

// console.log(chatListInstance);
const Farming = () => {
  const onChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setChatVal(e.target.value);
  };

  const [chatList, setChatList] = useState<chatList[]>(chatListInstance);

  const getAll = () => {
    getAllChat({ Uid: localStorage.getItem("Uid") }).then((res) => {
      setChatList(res);
    });
  };

  const [chatVal, setChatVal] = useState("");
  const [chatVal2, setChatVal2] = useState("");
  const postMsg = () => {
    console.log();
    console.log(moment().format("YYYY-MM-DD HH:mm:ss"));
    postChat({
      Uid: localStorage.getItem("Uid"),
      msg: chatVal,
      time: moment().format("YYYY-MM-DD HH:mm:ss"),
    }).then((res) => {
      console.log(res.output.text);
    });
  };

  const setBox = () => {
    setChatVal("现代农业如何发展");
  };

  useEffect(() => {
    getAll();
  }, []);
  return (
    <>
      <div className="farmingRoot">
        <div className="boxOfText" onClick={setBox}>
          .
        </div>
        <div className="boxOfText2">.</div>
        {/* 聊天对话框 */}
        <div className="chatBox">
          {chatList.map((item: chatList) => {
            return <ChatBoxItem data={item}></ChatBoxItem>;
          })}
        </div>
        {/* 聊天输入框 */}

        <Flex className="inputBox" vertical gap={32}>
          <TextArea
            allowClear
            showCount
            maxLength={100}
            onChange={onChange}
            placeholder="disable resize"
            style={{ height: 120, resize: "none" }}
          />
          <Button onClick={postMsg} className="inpBtn">
            发送
          </Button>
        </Flex>
      </div>
    </>
  );
};

export default Farming;

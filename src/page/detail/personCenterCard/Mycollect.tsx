import "./Mycollect.scss";
import type { stepPerson } from "../PersonCenter";
import RetrunLast from "./RetrunLast";
import { postAllCollect } from "@s/api/apis";
import { useEffect, useState } from "react";
import CardCollect from "../card/CardCollect";
import { cardT } from "../card/CardCom";
function Mycollect(props: { toSetStep: (val: stepPerson) => void }) {
  const { toSetStep } = props;
  const [dataList, setDataList] = useState<cardT[] | null>(null);
  const getAllCollect = () => {
    postAllCollect({
      Uid: localStorage.getItem("Uid"),
    }).then((res) => {
      setDataList(res as cardT[]);
    });
  };

  useEffect(() => {
    getAllCollect();
  }, []);
  return (
    <div className="MycollectRoot">
      <RetrunLast toSetStep={toSetStep}></RetrunLast>
      {dataList
        ? dataList.map((item: cardT, index: number) => {
            // TODO:瀑布流
            // if (index == 1)
            //   return (
            //     <div>
            //       <div
            //         style={{
            //           height: "50px",
            //         }}
            //       ></div>
            //       <CardCollect data={item}></CardCollect>
            //     </div>
            //   );

            return <CardCollect getAllCollect={getAllCollect} data={item}></CardCollect>;
          })
        : ""}
    </div>
  );
}

export default Mycollect;

import "./RetrunLast.scss";
import returnIcon from "@s/assets/returnIcon.png";
import { stepPerson } from "../PersonCenter";

export default function RetrunLast(props: {
  toSetStep: (val: stepPerson) => void;
}) {
  const { toSetStep } = props;
  const handleStep = () => {
    toSetStep(stepPerson.Main);
  };
  return (
    <img
      className="RetrunLastRoot"
      onClick={handleStep}
      style={{
        width: "40px",
        height: "35px",
      }}
      src={returnIcon}
    ></img>
  );
}

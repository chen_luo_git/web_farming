import "./MyArtical.scss";
import type { stepPerson } from "../PersonCenter";
import RetrunLast from "./RetrunLast";
import { postMyArtical } from "@s/api/apis";
import { useEffect, useState } from "react";
import { cardT } from "../card/CardCom";
import CardCollect from "../card/CardCollect";
function MyArtical(props: { toSetStep: (val: stepPerson) => void }) {
  const { toSetStep } = props;
  const [myArticalList, setMyArticalList] = useState<cardT[]>([]);

  function getMyArticalList() {
    postMyArtical({ userName: localStorage.getItem("userName") }).then(
      (res) => {
        setMyArticalList(res as cardT[]);
      }
    );
  }
  useEffect(() => {
    getMyArticalList();
  }, []);

  return (
    <div className="MyArticalRoot">
      <RetrunLast toSetStep={toSetStep}></RetrunLast>
      {myArticalList
        ? myArticalList.map((item: cardT, index: number) => {
            return (
              <CardCollect
                id="my"
                getAllCollect={getMyArticalList}
                data={item}
              ></CardCollect>
            );
          })
        : ""}
    </div>
  );
}

export default MyArtical;

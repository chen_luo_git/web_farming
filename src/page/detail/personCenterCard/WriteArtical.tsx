import "./WriteArtical.scss";
import { Button, Flex, Input } from "antd";
import type { stepPerson } from "../PersonCenter";
import { FormOutlined } from "@ant-design/icons";
import RetrunLast from "./RetrunLast";
import { useRef } from "react";
import { postWrite } from "@s/api/apis";
import moment from "moment";
import { cardT } from "../card/CardCom";

const { TextArea } = Input;
function WriteArtical(props: { toSetStep: (val: stepPerson) => void }) {
  const { toSetStep } = props;
  let titleWrite = useRef("");
  let conentWrite = useRef("");
  const writeProp: cardT = {
    title: "",
    abstract: "",
    date: "",
    author: localStorage.getItem("userName") + "",
    comment: 0,
    Aid: Number(localStorage.getItem("Aid")),
    content: "",
    isCollect: true,
    numCollect: 0,
  };

  function affirm() {
    writeProp.title = titleWrite.current;
    writeProp.abstract = conentWrite.current.substring(0, 50);
    writeProp.content = conentWrite.current;
    writeProp.date = moment().startOf("day").format("YYYY-MM-DD HH:mm:ss");
    console.log(writeProp);
    postWrite(writeProp);
  }
  const onChangeTitle = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    titleWrite.current = e.target.value;
  };
  const onChangeContent = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    conentWrite.current = e.target.value;
  };
  return (
    <div className="WriteArticalRoot">
      <RetrunLast toSetStep={toSetStep}></RetrunLast>
      {/* 标题 */}
      <Input
        className="titleWrite"
        size="large"
        placeholder="文章标题"
        onChange={onChangeTitle}
        prefix={<FormOutlined />}
      />
      {/* 文章主题 */}
      <TextArea
        ref={conentWrite as any}
        className="contentWrite"
        showCount
        maxLength={100}
        onChange={onChangeContent}
        placeholder="文章主体"
        style={{ height: 120, resize: "none" }}
      />
      <Button onClick={affirm}>确认提交</Button>
    </div>
  );
}

export default WriteArtical;

import http from "./http";

export const getTest = () => {
  return new Promise((res, rej) => {
    http("/home", "GET")
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 登录
export const postLogin = (data: any) => {
  return new Promise((res, rej) => {
    http("/login", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 注册
export const postSign = (data: any) => {
  return new Promise((res, rej) => {
    http("/sigin", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 获取用户信息
export const postUser = (data: any) => {
  return new Promise((res, rej) => {
    http("/user", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 获取文章
export const postAllArtical = (data: any) => {
  return new Promise((res, rej) => {
    http("/allArtical", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 收藏
export const postCollect = (data: any) => {
  return new Promise((res, rej) => {
    http("/collect", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 取消收藏
export const postNoCollect = (data: any) => {
  return new Promise((res, rej) => {
    http("/noCollect", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 用户收藏的文章
export const postAllCollect = (data: any) => {
  return new Promise((res, rej) => {
    http("/allCollect", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 用户写文章
export const postWrite = (data: any) => {
  return new Promise((res, rej) => {
    http("/writeArtical", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 我的文章
export const postMyArtical = (data: any) => {
  return new Promise((res, rej) => {
    http("/myArtical", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 观看了某一个文章【加浏览量】
export const postLook = (data: any) => {
  return new Promise((res, rej) => {
    http("/lookArtical", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 观看了某一个文章【加历史记录】
export const postLookAdd = (data: any) => {
  return new Promise((res, rej) => {
    http("/lookAdd", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// 观看历史
export const postLookHistory = (data: any) => {
  return new Promise((res, rej) => {
    http("/lookHistory", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 获取所有对话
export const getAllChat = (data: any) => {
  return new Promise((res, rej) => {
    http("/getChatList", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};
// 进行对话
export const postChat = (data: any) => {
  return new Promise((res, rej) => {
    http("/postChat", "POST", data)
      .then((resData) => {
        res(resData);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

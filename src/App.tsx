import { useEffect, useRef, useState } from "react";
import Home from "./page/Home";
import Login from "./page/Login";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { getTest } from "@api";

function App() {
  // const llamaApi = () => {
  //   fetch(
  //     "https://dashscope.aliyuncs.com/api/v1/services/aigc/text-generation/generation",
  //     {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "sk-f7781b216b88400fa20270cfc92260a4",
  //       },
  //       // body: JSON.stringify(data),
  //       body: JSON.stringify({
  //         model: "llama2-7b-chat-v2",
  //         input: {
  //           prompt: "hello, who are you?",
  //           messages: [
  //             { content: "Where is the capital of Zhejiang?", role: "user" },
  //           ],
  //         },
  //       }),
  //     }
  //   )
  //     .then((res) => {
  //       console.log(res, "模型");
  //     })
  //     .then((res) => {
  //       console.log(res, "模型");
  //     })
  //     .catch((error) => {
  //       console.error("Error:", '模型');
  //     });
  // };

  const [isLogin, setIsLogin] = useState<boolean>(false);
  const setLogin = (val: boolean) => {
    setIsLogin(val);
  };

  let count = 0;
  let deep = 0;
  // function countAllNodeAndDeepNode(nodeTree, deepO) {
  //   if (nodeTree?.childNodes?.length !== 0) {
  //     // 每一项进行递归，判断节点
  //     Array.from(nodeTree.childNodes).map((item) => {
  //       countAllNodeAndDeepNode(item, deepO + 1);
  //     });
  //   }
  //   count++;
  //   deep < deepO ? (deep = deepO) : (deep = deep);
  //   console.log("当前节点数量", count, "当前节点深度", deep);
  // }
  const tree = useRef<HTMLElement>(null);
  // useEffect(() => {
  //   if (tree.current !== null) {
  //     countAllNodeAndDeepNode(tree.current, 0);
  //   }
  // }, [tree]);

  //测试
  useEffect(() => {
    // llamaApi();
    getTest().then((res) => {
      // console.log(res);
    });
  }, []);
  count = count + 50;
  return (
    <div ref={tree}>
      <BrowserRouter>
        {isLogin ? (
          <Home setLogin={setLogin}></Home>
        ) : (
          <Login setLogin={setLogin}></Login>
        )}
        {/* <Home></Home> */}
        {/* <Login></Login> */}
      </BrowserRouter>
    </div>
  );
}

export default App;

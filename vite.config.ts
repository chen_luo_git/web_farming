import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@p": path.resolve(__dirname, "public"),
      "@u": path.resolve(__dirname, "utils"),
      "@s": path.resolve(__dirname, "src"),
      "@api": path.resolve(__dirname, "src/api/apis"),
    },
  },
});
